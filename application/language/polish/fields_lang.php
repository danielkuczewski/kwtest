<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	Daniel Kuczewski
 * @copyright	Copyright (c) 2017, Daniel Kuczewski (http://zaawansowanywordpress.pl/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['field_first_name']  = 'Imię';
$lang['field_last_name']  = 'Nazwisko';
$lang['field_address']  = 'Adres';
$lang['field_password']  = 'Hasło';