<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	Daniel Kuczewski
 * @copyright	Copyright (c) 2017, Daniel Kuczewski (http://zaawansowanywordpress.pl/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang["site_name"] = "System przedszkole";

$lang["facebook_login"] = "Zaloguj przez facebook";

$lang["start"] = "Start";
$lang["users"] = "Użytkownicy";
$lang["list"] = "Lista";
$lang["group"] = "Grupy";
$lang["notifications"] = "Powiadomienia";
$lang["quantity"] = "ilość";
$lang["id"] = "ID";
$lang["name"] = "Nazwa";
$lang["username"] ="Login";
$lang["username_or_email"] = "Login lub email";
$lang["phone"] = "Numer telefonu";
$lang["first_name"] = "Imię";
$lang["last_name"] = "Nazwisko";
$lang["email"] = "E-mail";
$lang["address"] = "Adres";
$lang["password"] = "Hasło";
$lang["password_repeat"] ="Powtórz hasło";
$lang["post_code"] = "Kod pocztowy";
$lang["city"] = "Miasto";
$lang["street"] = "Ulica";
$lang["captcha"] = "Kod z obrazka";
$lang["active"] = "Aktywny";
$lang["no_active"] = "Konto nie aktywne (wyślji mail aktywacyjny)";

$lang["panel"] = "Panel";
$lang["admin"] = "Administrator";
$lang["teacher"] = "Nauczyciel";
$lang["teacherss"] = "nauczycieli";
$lang["parent"] = "Rodzic";
$lang["parents"] = "Rodzice";
$lang["parent_added"] = "Dodano rodzica";
$lang["parent_welcome"] = "Wiaj w panelu rodzica";

$lang["groupe"] = "grupę";
$lang["group"] = "Grupa";
$lang["grup"] = "Grupy";
$lang["add_group"] = "Dodaj grupę";
$lang["group_add_success"] = "Dodano grupę!";
$lang["group_edit_success"] = "Uaktualniono grupę!";
$lang["list_all_children"] = "Zarządzaj grupą";

$lang["teachers"] = "Nauczyciele";

$lang["added"] = "Dodano";
$lang["edited"] = "Edytowano";
$lang["data"] = "dane";
$lang['add']  = 'Dodaj';
$lang['edit']  = 'Edytuj';
$lang['delete'] = "Usuń";
$lang["save"] = "Zapisz";
$lang["see"] = "Zobacz";
$lang["back"] = "Wróć";
$lang["usera"] = "użytokwnika";
$lang['child'] = 'Dziecko';
$lang["children"] ="Dzieci";
$lang["logout"] ="Wyloguj się";

$lang["child_changed_status"] = "Zmieniono status dziecka na: ";
$lang["child_data"] = "Dane dziecka";
$lang["group_id"] = "Identyfikator grupy";
$lang["child_added_to_group"] = "Dodano dziecko do grupy.";
$lang["teacher_added_to_group"] = "Dodano nauczyciela do grupy.";
$lang["bad_child_id"] = "Nieprawidłowy identyfikator dziecka";
$lang["edit_name"] = "Zmień nazwę";

$lang["lack_account"] = "Logowanie nieudane, spróbuj się zalogować bez facebooka.";
$lang["lack_notifications"] = "Brak powiadomień";
$lang["notify_content"] = "Treść powiadomienia";
$lang["notify_added"] = "Dodano powiadomienie";
$lang["new_notify"] = "Nowe powiadomienie";

$lang["type"] = "typ";
$lang["status"] = "Status";
$lang["action"] = "Akcja";
$lang["lack"] = "Brak";

$lang["year"] = "Rok";
$lang["month"] = "Miesiąc";
$lang["day"] = "Dzień";
$lang["birth"] = "urodzenia";
$lang["date"] = "Data";

$lang["login_header"] = "Logowanie";
$lang["signin"] = "Zaloguj się";
$lang["login_ok"] = "Zalogowano do systemu";
$lang["signup"] = "Zarejestruj się";
$lang["register"] = "Rejestracja";
$lang["register_ok"] = "Rejestracja udana, na twój adres email został wysłany link aktywacyjny"; 
$lang["password_content_cloud"] = "Hasło powinno mieć co najmniej 6 znaków, małą i dużą literę, cyfrę i co najmniej jeden znak specjalny.";

$lang["account_activation"] = "Aktywacja konta";
$lang["account_activation_email_content"] = "Własnie zarejestrowałeś się w systemie zarządzania przedszkolem aby aktywować konto kliknij poniższy link";
$lang["remind_password"] = "Przypomnienie hasła";
$lang["remind_password_description"] = "Nie pamiętasz hasła? Wpisz swój adres e-mail w poniższe pole a my wygenerujemy specjalny link, dzieki któremu będziesz mógł ustalić nowe hasło i prześlemy na podany adres. Pamiętaj jednak że link jest ważny przez godzinę.";
$lang["remind_send"] = "Link do zmiany hasła został wysłany na twój adres email.";
$lang["remind_password_email_content"] = "Aby ustalić nowe hasło przejdź pod następujący adres: ";
$lang["bad_remind_key"] = "Ten link już wygasł, skorzystaj ponownie z przypomnienia hasła aby wygenerować nowy. Link jest ważny tylko godzinę.";
$lang["new_password"] = "Nowe hasło";
$lang["remind_checked"] = "Twoje hasło zostało zmienione.";
$lang["change_password_description"] = "Poniżej możesz ustanowić nowe hasło dla swojego konta.";

$lang["want_delete_user"] = "Czy napewno chcesz usunąć tego użytkownika?";
$lang["want_delete_group"] = "Czy napewno chcesz usunąć tę grupę? ";
$lang["want_delete_child"] = "Czy napewno chcesz usunąć to dziecko, zostaną usunięte także wszystkie inne dane z nim związane? ";
$lang["want_delete_child_from_group"] = "Czy chcesz usunąć dziecko z tej grupy?";
$lang["want_delete_teacher_from_group"] = "Czy chcesz usunąć nauczyciela z tej grupy?";
$lang["want_delete_parent"] = "Czy chcesz usunąć rodzica?";
$lang["want_delete_notify"] = "Czy chcesz usunąć powiadomienie?";

//statusy dzieci
$lang["child_status_0"] = "Oczekuje";
$lang["child_status_2"] = "Przyjęty";

$lang["user_status_0"] = "Nieaktywny";
$lang["user_status_1"] = "Aktywny";

$lang["user_type_0"] = "Rodzic";
$lang["user_type_"] = "Rodzic";
$lang["user_type_teacher"] = "Nauczyciel";
$lang["user_type_admin"] = "Administrator";


$lang["ajax_add_child_success"] = "Dodano dziecko do systemu";
$lang["ajax_edit_child_success"] = "Edytowano pomyślnie dane dziecka";

$lang["bad_token"] = "Zły token";
$lang["lack_function"] = "Brak funkcji";