<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['form_validation_required']               = 'Pole {field} jest wymagane.';  
$lang['form_validation_isset']                  = 'Pole {field} musi posiadać wartość.';
$lang['form_validation_valid_email']            = 'Pole {field} musi posiadać poprawny adres e-mail.';
$lang['form_validation_valid_emails']           = 'Pole {field} musi posiadać poprawne adresy e-mail.';
$lang['form_validation_valid_url']              = 'Pole {field} musi posiadać poprawny adres URL.';
$lang['form_validation_valid_ip']               = 'Pole {field} musi posiadać poprawny adres IP.';
$lang['form_validation_min_length']             = 'Długość pola {field} nie może być mniejsza niż {param}.';
$lang['form_validation_max_length']             = 'Długość pola {field} nie może być większa niż {param}.';
$lang['form_validation_exact_length']           = 'Pole {field} musi zawierać dokładnie {param} znak(i).';
$lang['form_validation_alpha']                  = 'Pole {field} może zawierać tylko litery.';
$lang['form_validation_alpha_numeric']          = 'Pole {field} może zawierać tylko znaki alfanumeryczne.';
$lang['form_validation_alpha_numeric_spaces']   = 'Pole {field} może zawierać tylko znaki alfanumeryczne i spacje.';
$lang['form_validation_alpha_dash']             = 'Pole {field} może zawierać tylko znaki alfanumeryczne, podkreślenia i myślniki.';
$lang['form_validation_numeric']                = 'Pole {field} może zawierać tylko liczby.';
$lang['form_validation_is_numeric']             = 'Pole {field} może zawierać tylko znaki numeryczne.';
$lang['form_validation_integer']                = 'Pole {field} może zawierać tylko liczby całkowite.';
$lang['form_validation_regex_match']            = 'Pole {field} ma nieodpowiedni format.';
$lang['form_validation_matches']                = 'Pole {field} ma inną wartość niż pole {param}.';
$lang['form_validation_differs']                = 'Pole {field} musi mieć inną wartość niż pole {param}.';
$lang['form_validation_is_unique']              = 'Pole {field} musi posiadać unikalną wartość.';
$lang['form_validation_is_natural']             = 'Pole {field} musi zawierać tylko wartości dodatnie.';
$lang['form_validation_is_natural_no_zero']     = 'Pole {field} musi zawierać tylko wartości większe od zera.';
$lang['form_validation_decimal']                = 'Pole {field} musi zawierać liczbę dziesiętną.';
$lang['form_validation_less_than']              = 'Pole {field} musi zawierać liczbę mniejszą niż {param}.';
$lang['form_validation_less_than_equal_to']     = 'Pole {field} musi zawierać liczbę mniejszą lub równą {param}.';
$lang['form_validation_greater_than']           = 'Pole {field} musi zawierać liczbę większą niż {param}.';
$lang['form_validation_greater_than_equal_to']  = 'Pole {field} musi zawierać liczbę większą lub równą {param}.';
$lang['form_validation_error_message_not_set']  = 'Nie można uzyskać dostępu do komunikatu o błędzie dla pola {field}.';
$lang['form_validation_in_list']                = 'Pole {field} musi mieć jedną z następujących wartości: {param}.';

$lang['form_validation_check_recaptcha'] = "Nieprawidłowy kod z obrazka.";
$lang['form_validation_check_recaptchag'] = "Przejdź test reCaptcha.";
$lang['form_validation_check_status'] = "Twoje konto nie zostało jeszcze aktywowane.";
$lang['form_validation_check_password_login'] ="Nieprawidłowe hasło lub email.";
$lang['form_validation_check_username_login'] ="Brak takiego adresu email.";
$lang['form_validation_check_password_number'] ="Hasło musi zawierać cyfrę.";
$lang['form_validation_check_password_small_char'] ="Hało musi zawierać małą literę.";
$lang['form_validation_check_password_big_char'] ="Hało musi zawierać dużą literę.";
$lang['form_validation_check_password_special_char'] ="Hało musi zawierać znak specjalny.";
$lang['form_validation_check_email'] ="Ten email jest już w naszej bazie, skorzystaj z przypomnienia hasła.";
$lang['form_validation_check_username'] ="Ten email nie jest dostępny.";
$lang['form_validation_check_child'] = "Takie dziecko już istnieje w naszej bazie!";
$lang['form_validation_check_email_remind_password'] = "Niestety nie posidamy takiego adresu email w naszej bazie.";
$lang['form_validation_check_group'] = "Grupa o takiej nazwie już istnieje.";
$lang["form_validation_exists_child"] = "Wpis musi zaczynać się od identyfikatora. Brak dziecka.";
$lang["form_validation_child_in_group"] = "Takie dziecko jest już w tej grupie"; 
$lang["form_validation_exists_teacher"] = "Brak nauczyciela.";
$lang["form_validation_teacher_in_group"] = "Taki nauczyciel jest już przypisany do tej grupy";
$lang["form_validation_check_user"] = "Brak użytkownika o takim identyfikatorze.";
$lang["form_validation_check_is_parent"] = "Ten użytkownik już jest rodzicem tego dziecka.";