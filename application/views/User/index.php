<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <h2>Panel i ankieta</h2>
            <p>
                W tym panelu możesz wypełnić prostą ankietę.<br>
                Wszystkie pola są obowiązkowe.<br>
                Ankietę może wypełnić nieskończoną ilość razy.<br>
            </p>
            <?php if($saved){ ?>
            <div class="alert alert-success">
                Twoja ankieta została zapisana.
            </div>
            <?php } ?>
            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger">
                    <?= validation_errors(); ?>
                </div>
            <?php } ?>
            <?= form_open(); ?>
            <?php if(!empty($questions[$question-1])) { ?>
            <p>
                <b><?= $questions[$question-1][1];?></b>
            </p>
            <div class="form-group">
                <?= form_hidden(array(
                    "name" => "qid",
                    "value" => $questions[$question-1][0]
                ));?>
                <?=
                form_textarea(array(
                    "name" => "answer",
                    "value" => !empty($answers[$question-1][3]) ? $answers[$question-1][3] : "",
                    "placeholder" => "Twoja odpowiedź *",
                    "required" => "TRUE",
                    "class" => "form-control"
                ));
                ?>
            </div>
            <div class="form-group">
                <?php if($question > 1) { ?>
                <a href="<?= base_url("user/index/".($question - 1));?>">Cofnij</a>
                <?php } ?>
                <?=
                form_submit(array(
                    "name" => "send",
                    "value" => "Zapisz i przejdź dalej",
                    "class" => "btn btn-primary"
                ));
                ?>
            </div>
            <?= form_close(); ?>
            <?php } else { ?>
            <?= form_open();?>
            <p>
                To już koniec tej ankiety.
            </p>
            <?= form_submit(array(
                "name" => "save",
                "value" => "Zapisz i przejdź do kolejnej ankiety",
                "class" => "btn btn-primary"
            ));?>
            <?= form_close();?>
            <?php } ?>
        </div>
    </div>
</div>