
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <h2>Formularz rejestracji</h2>
            <?php if (validation_errors()) { ?>
                <div class="alert alert-danger">
                    <?= validation_errors(); ?>
                </div>
            <?php } ?>
            <?= form_open(); ?>
            <div class="form-group">
                <?=
                form_input(array(
                    "name" => "email",
                    "value" => !empty($_POST["email"]) ? $_POST["email"] : "",
                    "placeholder" => "E-mail *",
                    "required" => "TRUE",
                    "class" => "form-control"
                ));
                ?>
            </div>
            <div class="form-group">
                <?=
                form_password(array(
                    "name" => "password",
                    "placeholder" => "Hasło *",
                    "required" => "TRUE",
                    "class" => "form-control"
                ));
                ?>
            </div>
            <div class="form-group">
                <?=
                form_password(array(
                    "name" => "password_repeat",
                    "placeholder" => "Powtórz hasło *",
                    "required" => "TRUE",
                    "class" => "form-control"
                ));
                ?>
            </div>
            <div class="form-group">
                <div class="g-recaptcha" data-sitekey="6Lc_KyoUAAAAAEJHpzigw-HMLFvGlTGAv9K11gRN"></div>
            </div>
            <div class="form-group">
                <?=
                form_submit(array(
                    "name" => "send",
                    "value" => "Zarejestruj się",
                    "class" => "btn btn-primary"
                ));
                ?>
            </div>
            <?= form_close(); ?>
        </div>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>