
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Witaj w KW Test!</h1>
        <p>Prosta apka testowa dająca możliwość przeprowadzenia ankiety dwupytaniowej a następnie przejrzenia wyników.</p>
        <p><a class="btn btn-primary btn-lg" href="<?= base_url("admin/index");?>" role="button">Przejdź do admina &raquo;</a></p>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4">
          <h2>Dwa pytania</h2>
          <p>Tylko dwa pytania i nic więcej ze strony użytkownika.</p>
          <p><a class="btn btn-default" href="<?= base_url("user/index");?>" role="button">Odpowiedz na pytania &raquo;</a></p>
        </div>
        <div class="col-md-4">
          <h2>Prosta rejestracja</h2>
          <p>Bardzo łatwa rejestracja zajmująca najwyżej dwie minuty </p>
          <p><a class="btn btn-default" href="<?= base_url("main/register");?>" role="button">Zarejestruj się &raquo;</a></p>
       </div>
        <div class="col-md-4">
          <h2>Łatwe logowanie</h2>
          <p>Zupełnie standardowe logowanie. Zaloguj się by odpowiadać na ankiety.</p>
          <p><a class="btn btn-default" href="<?= base_url("main/login");?>" role="button">Zaloguj się &raquo;</a></p>
        </div>
      </div>

      <hr>

      