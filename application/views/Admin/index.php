<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2>Odpowiedzi użytkowników</h2>
            <p>
                Poniżej znajduje się tabela ze wszystkimi odpowiedziami użytkowników.
            </p>
            <div class="table-responsive">
            <table class="table  table-striped table-bordered"  data-toggle="table" data-pagination="true" >
                <thead>
                    <tr>
                        <th data-field="id" data-sortable="true">ID</th>
                        <!--<th>AID</th>-->
                        <th data-field="email" data-sortable="true">Email</th>
                        <th data-field="first_last_name" data-sortable="true">Imie i nazwisko</th>
                        <th data-field="age" data-sortable="true">Wiek</th>
                        <th data-field="created_at" data-sortable="true">Data wypełnienia</th>
                    </tr>
                </thead>
                <tbody id="myTable">
                    <?php foreach ($answers as $answer) { ?>
                    <tr>
                        <td><?= $answer[0];?></td>
                        <td><?= $answer[1];?></td>
                        <td><?= $answer[3][0];?></td>
                        <td><?= $answer[3][1];?></td>
                        <td><?= $answer[2];?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>