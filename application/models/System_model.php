<?php

class System_model extends CI_Model {
    private $table = "questions";
    
    function __construct() {
        parent::__construct();
    }
    
    public function questions(){
        $q = $this->db->get($this->table);
        $questions = array();
        foreach($q->result() as $r){
            $questions[] = [$r->id,$r->content];
        }
        return $questions;
    }
    public function save_answers($user_id, $aid, $answers){
        
        $this->db->insert("aids", array("aid" => $aid));
        foreach($answers as $answer){
            if(!empty($answer[0])){
                $this->db->insert("answers",array(
                   "user_id" => $user_id,
                    "question_id" => $answer[0],
                    "answer" => $answer[2],
                    "aid" => $aid
                ));
            }
        }
        
        
        $this->session->unset_userdata("answers");
        $this->session->unset_userdata("aid");
        return TRUE;
    }
    
    //pobiera wszystkie odpowiedzi w tablicy 
    public function get_answers(){
        $q1 = $this->db->get("aids");
        $answers = [];
        foreach($q1->result() as $aid){
            $q = $this->db->join("users","users.id = answers.user_id")->get_where("answers", array("aid" => $aid->aid));
            $answer = [];
            foreach($q->result() as $r){
                $answer[] = $r->answer;
            }
            $answers[] = [$aid->id,$r->email, $r->created_at, $answer];
        }
        return $answers;
    }
}

