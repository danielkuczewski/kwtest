<?php
class User_model extends CI_Model {
    private $table = "users";
    
    function __construct() {
        parent::__construct();
    }
    
    public function register_user($email,$password, $status = 0){
        $pass = sha1($password);
        $this->db->insert($this->table, array("email" => $email, "password" => $pass, "status" => $status));
        return $this->db->insert_id();
    }
    
    public function login_user($email, $password){
        $q = $this->db->get_where($this->table, array("email" => $email, "password" => sha1($password)));
        if($q->num_rows()){
            $user = $q->row();
            $user = array(
                "user_id" => $user->id,
                "email" => $user->email,
                "status" => $user->status,
                "logged_in" => "true"
            );
            $this->session->set_userdata($user);
            return TRUE;
        } else {
            return FALSE;
        }
    }
    
    public function logout(){
        $this->session->unset_userdata("email");
        $this->session->unset_userdata("status");
        $this->session->unset_userdata("logged_in");
    }
}
