<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public function __construct() {
            parent::__construct();
        }
        
        public function admin_view($view, $data = array()){
            $this->load->view("Main/header");
            $this->load->view("Admin/".$view, $data);
            $this->load->view("Main/footer");
        }
        
        public function user_view($view, $data = array()){
            $this->load->view("Main/header");
            $this->load->view("User/".$view, $data);
            $this->load->view("Main/footer");
        }
        
        public function main_view($view, $data = array()){
            $this->load->view("Main/header");
            $this->load->view("Main/".$view, $data);
            $this->load->view("Main/footer");
        }
}