<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function __construct() {
            parent::__construct();
            //ten kontroler wchodzi tylko jeśli zalogowany
            if(!is_logged()){
                redirect(base_url("main/login"));
            }
        }
        
        public function index($question = 1){
            //generuję identyfikator sessji jeśli nie ma
            if(empty($this->session->aid)){
                $aid = $this->session->user_id . date('U');
                $this->session->set_userdata(array("aid" => $aid));
            } else {
                $aid = $this->session->aid;
            }
            //pobieram wszystkie pytania
            $questions = $this->System_model->questions();
            
            //pobieram odpowiedzi
            if(empty($this->session->answers)){
                $answers = $questions;
                $this->session->set_userdata(array("answers" => $answers));
            } else {
                $answers = $this->session->answers;
            }
            
            //zapisuję odpowiedź i przechodzę do następnego
            if($this->input->post("send")){
                $this->form_validation->set_rules('answer', 'odpowiedź', 'required');
                if($this->form_validation->run()){
                    $answers[$question-1][2] = $this->input->post("answer");
                    $this->session->answers = $answers;
                    redirect(base_url("user/index/".($question+1)));
                }
            }
            
            //zapisuję ankietę
            if($this->input->post("save")){
                $this->System_model->save_answers($this->session->user_id, $aid, $answers);
                $this->session->set_userdata(array("saved" => "true"));
                redirect(base_url("user/index"));
            }
            $saved = false;
            if(!empty($this->session->saved)){
                $saved = true;
                $this->session->unset_userdata("saved");
            }
            
            
            $this->user_view("index",array(
                "questions" => $questions,
                "aid" => $aid,
                "question" => $question,
                "answers" => $answers,
                "saved" => $saved
            ));
        }
}
