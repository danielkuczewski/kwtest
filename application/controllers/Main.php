<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

	public function __construct() {
            parent::__construct();
            //ten kontroler wchodzi bez żadnej autoryzacji
        }
        
        public function index(){
            $this->main_view("index");
        }
        
        public function register(){
            if(is_logged()){
                redirect(base_url("user/index"));
            }
            if($this->input->post("send")){
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
                $this->form_validation->set_rules('password', 'Hasło', 'required');
                $this->form_validation->set_rules('password_repeat', 'Powtórz hasło', 'required|matches[password]');
                $this->form_validation->set_rules('g-recaptcha-response', 'reCaptcha', 'callback__validate_kapcza');
                $this->form_validation->set_message('_validate_kapcza', "Musisz przejść test reCaptcha");
                if($this->form_validation->run()){
                    $this->User_model->register_user($this->input->post("email"), $this->input->post("password"));
                    $this->main_view("register_success");
                } else {
                    $this->main_view("register");
                }
 
            } else {
                $this->main_view("register");
            }
        }
        
        public function login(){
            if(is_logged()){
                redirect(base_url("user/index"));
            }
            if($this->input->post("send")){
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
                $this->form_validation->set_rules('password', 'Hasło', 'required|callback_user_login');
   
                $this->form_validation->set_message('user_login', "Niepoprawny login lub hasło");
                if($this->form_validation->run()){
                    
                    redirect(base_url("user/index"));
                } else {
                    $this->main_view("login");
                }
 
            } else {
                $this->main_view("login");
            }
        }
        
        public function logout(){
            $this->User_model->logout();
            redirect(base_url("main/login"));
        }
        
        public function user_login(){
            return $this->User_model->login_user($this->input->post("email"), $this->input->post("password"));
        }
        
        public function _validate_kapcza($str)
	{
            $google_url = "https://www.google.com/recaptcha/api/siteverify";
            $secret = "6Lc_KyoUAAAAAHGXurFyoMPcyIZeTEidDwgwhgBu";
            $ip = $_SERVER['REMOTE_ADDR'];
            $url = $google_url . "?secret=" . $secret . "&response=" . $str . "&remoteip=" . $ip;
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  //important for localhost!!!
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_TIMEOUT, 10);
            curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.16) Gecko/20110319 Firefox/3.6.16");
            $res = curl_exec($curl);
            curl_close($curl);
            $res = json_decode($res, true);
            //reCaptcha success check
            if ($res['success']) {
                return TRUE;
            } else {
                return FALSE;
            }
	}
        
}