<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function __construct() {
            parent::__construct();
            //ten kontroler wchodzi tylko jeśli user ma status powyżej 0
            if(!is_logged() || $this->session->status == 0){
                redirect(base_url("user/index"));
            }
        }
        
        public function index(){
            //pobieram dane do wyświetlenia w tabelce
            $answers = $this->System_model->get_answers();
            
            
            $this->admin_view("index",array(
                "answers" => $answers
            ));
        }
}
